﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextMovement : MonoBehaviour {

    public Text text;
    Vector3 startPosition;
    Vector3 movement = new Vector3(0, 1, 0);
    public int count;

	// Use this for initialization
	void Start () {
        startPosition = text.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (count == 1)
        {
            text.transform.position += movement;
            count -= 1;
        }
        if (count == 0)
        {
            text.transform.position -= movement;
            count += 1;
        }
	}
}
