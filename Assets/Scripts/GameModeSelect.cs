﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameModeSelect : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void startGameA()
    {
        ScoreManager.S.changeMode(0);
        SceneManager.LoadScene("Level_forSpawnerB");
    }

    public void startGameB()
    {
        ScoreManager.S.changeMode(1);
        SceneManager.LoadScene("Level_forSpawnerB");
    }
}
