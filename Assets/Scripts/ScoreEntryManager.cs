﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreEntryManager : MonoBehaviour
{

    private string saveEntryName = "";
    public Text entryText;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            GoToGameOver();
        }
    }

    public void GoToGameOver()
    {
        saveEntryName = entryText.text;

        // Save score if player entered a name
        if (saveEntryName != "")
        {
            ScoreManager.S.saveCurrScore(saveEntryName);
        }
        SceneManager.LoadScene("GameOverScene");
    }
}
