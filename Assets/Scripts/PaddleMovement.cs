﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMovement : MonoBehaviour {

    private float PlayerSpeed = 35;
    private float widthLimit = 7f;

    private Vector3 movement;
    //private Vector3 prevPos;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

        //prevPos = gameObject.transform.position;

        movement = new Vector3(Input.GetAxis("Horizontal"), 0, 0) * PlayerSpeed * Time.deltaTime;
        gameObject.transform.position += movement;


        // Ensure that the paddle does not move out of bounds
        if (gameObject.transform.position.x >= widthLimit)
        {
            gameObject.transform.position = new Vector3(widthLimit, gameObject.transform.position.y, gameObject.transform.position.z);
        }
        else if (gameObject.transform.position.x <= -1f *widthLimit)
        {
            gameObject.transform.position = new Vector3(-1f * widthLimit, gameObject.transform.position.y, gameObject.transform.position.z);
        }

    }
}
