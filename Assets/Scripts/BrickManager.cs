﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickManager : MonoBehaviour
{

    private Color currColor;
    private Material mat;
    public bool shiftColor = false;

    private Vector3 constMovement = new Vector3(0, 0, -1);

    // Use this for initialization
    void Start()
    {
        mat = GetComponent<Renderer>().material;
        currColor = mat.GetColor("_Color");
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += constMovement * Time.deltaTime;

        if (gameObject.transform.position.z < -6)
        {
            ScoreManager.S.changeScore(-1);
            Destroy(gameObject);
        }

        if (shiftColor)
        {
            changeColor();
        }

    }

    void changeColor()
    {
        currColor.r += 0.0004f;
        currColor.b -= 0.0004f;
        mat.SetColor("_Color", currColor);
    }
}
