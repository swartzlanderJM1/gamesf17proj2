﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrintHighScores : MonoBehaviour
{
    public Text HighScoreNameText;
    public Text HighScoreValText;
    public Text HighScoreHeader;

    // Use this for initialization
    void Start()
    {

        // Find maximum index to search
        int max = Mathf.Min(ScoreManager.S.getScoreListCount(), 10);

        string nameList = "";
        string valList = "";
        for (int i = 1; i <= max; i++)
        {
            nameList += ScoreManager.S.getScoreListName(i);
            valList += ScoreManager.S.getScoreListVal(i);

            if (i != max)
            {
                nameList += '\n';
                valList += '\n';
            }
        }
        HighScoreNameText.text = nameList;
        HighScoreValText.text = valList;
        if (ScoreManager.S.scoreMode == 0)
        {
            HighScoreHeader.text = "Mode A High Scores:";
        }
        else
        {
            HighScoreHeader.text = "Mode B High Scores:";
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
