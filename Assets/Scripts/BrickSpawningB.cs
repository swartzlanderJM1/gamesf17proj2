﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BrickSpawningB : MonoBehaviour
{
    private IEnumerator endingCoroutine;
    bool endState = false;

    public GameObject brickPrefab;

    private int[,] spawnList = new int[,]{  {1,0,0,0,0,0,0,1},
                                            {0,1,0,0,0,0,1,0},
                                            {0,0,1,0,0,1,0,0},
                                            {0,0,0,1,1,0,0,0},
                                            {1,1,1,1,1,1,1,1},
                                            
                                            {0,0,1,0,0,0,0,0},
                                            {1,0,0,0,0,0,0,0},
                                            {0,0,0,1,0,0,0,0},
                                            {1,0,0,0,1,0,0,0},
                                            {0,0,0,0,0,0,0,1},
                                            {0,1,0,0,0,0,0,0},
                                            {0,0,0,0,0,1,0,0},
                                            {0,0,0,0,1,0,0,0},
                                            {0,0,1,0,0,0,1,0},
                                            
                                            {0,1,0,0,0,0,0,0},
                                            {0,1,0,0,0,1,0,0},
                                            {0,1,1,0,0,1,0,0},
                                            {0,1,1,0,0,1,0,0},
                                            {0,0,1,0,0,1,0,1},
                                            {0,0,1,0,0,0,0,1},
                                            {0,0,0,0,0,0,0,1},
                                            {0,0,0,0,0,0,0,1}
    };


    private float[] spawnTimes = {  0, 0.5f, 1.0f, 1.5f, 2.5f,
                                    10, 11, 12, 13, 14, 15, 16, 17, 18,
                                    30.0f, 31.5f, 33.0f, 34.5f, 36.0f, 37.5f, 39.0f, 40.5f};
    private float currTime;

    // Use this for initialization
    void Start()
    {
        endingCoroutine = EndOfSceneChecker();
    }

    // Update is called once per frame
    void Update()
    {
      
        currTime += Time.deltaTime;

        for (int i = 0; i < spawnTimes.Length; i++)
        {
            if (spawnTimes[i] >= 0 && spawnTimes[i] < currTime)
            {
                spawnTimes[i] = -1; // Mark off time as passed
                Spawn(i);
            }
        }

        // If all blocks have been spawned, begin to check for game end
        if (endState == false && spawnTimes[spawnTimes.Length - 1] == -1)
        {
            StartCoroutine(endingCoroutine);
            endState = true;
        }
    }

    private void Spawn(int listIndex)
    {
        for (int i = 0; i < spawnList.GetLength(1); i++)
        {
            if (spawnList[listIndex, i] == 1)
            {
                GameObject newBrick = Instantiate(brickPrefab);
                newBrick.transform.position = new Vector3(2.2f * (i - 5) + 3.3f, 1.5f, 30f);
            }
        }
    }

    private IEnumerator EndOfSceneChecker()
    {
        while (true)
        {
            GameObject[] allBricks = GameObject.FindGameObjectsWithTag("Brick");
            if (allBricks.Length == 0)
            {
                SceneManager.LoadScene("ScoreEntryScene");
                yield return null;
            }
            yield return new WaitForSecondsRealtime(0.05f);
        }
    }
}
