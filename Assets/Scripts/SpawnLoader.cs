﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLoader : MonoBehaviour {

    public GameObject brickPrefab;

	// Use this for initialization
	void Start () {
        if (ScoreManager.S.scoreMode == 0)
        {
            BrickSpawning spawner = gameObject.AddComponent<BrickSpawning>();
            spawner.brickPrefab = brickPrefab;
        }
        else
        {
            BrickSpawningB spawner = gameObject.AddComponent<BrickSpawningB>();
            spawner.brickPrefab = brickPrefab;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
