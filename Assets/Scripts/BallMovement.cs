﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour {

    private GameObject clicker;
    private GameObject brickImpactSound;

    public bool inPlay;

    private static float ballInitSpeedVal = 400f;
    private Vector3 ballInitVelocity = new Vector3(0,0,ballInitSpeedVal);
    private Vector3 initPos;

    private bool buttonReleased = true;
    private bool canRecall = true;
    private const float cooldownMaxTime = 3;
    private IEnumerator cooldownRoutine;

    private Rigidbody rb;
    private Transform paddle;

    Material paddleMat;
    Color initPaddleColor;
    Color coolPaddleColor = new Color(0,0,0,1);

    // Use this for initialization
    void Start () {
        clicker = (GameObject)Resources.Load("ClickSoundPlayer");
        brickImpactSound = (GameObject)Resources.Load("ThudSoundPlayer");
    }

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        paddle = gameObject.transform.parent;

        cooldownRoutine = PaddleCooldown();

        paddleMat = GameObject.FindGameObjectWithTag("PaddleFake").GetComponent<Renderer>().material;
        initPaddleColor = paddleMat.GetColor("_Color");

        initPos = gameObject.transform.localPosition;
        inPlay = false;
        canRecall = true;
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetAxis("Fire1") != 1)
        {
            buttonReleased = true;
        }

        // Ball is on paddle
        if (inPlay == false && Input.GetAxis("Fire1") == 1 && buttonReleased == true)
        {
            this.transform.parent = null; // Unbind from the paddle
            rb.isKinematic = false;
            rb.AddForce(ballInitVelocity);
            inPlay = true;
            buttonReleased = false;
        }

        // Ball goes out of play
        if (inPlay == true && gameObject.transform.position.z < (paddle.gameObject.transform.position.z - 2))
        {
            Recall();
        }

        if (inPlay == true && canRecall == true && Input.GetAxis("Fire1") == 1 && buttonReleased == true)
        {
            StopCoroutine(cooldownRoutine);
            // Don't restart cooldown if already past paddle
            if (this.transform.position.z > paddle.transform.position.z)
            {
                canRecall = false;
                cooldownRoutine = PaddleCooldown();
                StartCoroutine(cooldownRoutine);
            }

            Recall();
            buttonReleased = false;
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Brick")
        {
            GameObject sound = Instantiate(clicker);
            sound.transform.position = collision.gameObject.transform.position;
            // Remove the brick
            Destroy(collision.gameObject);
            // Increment score
            ScoreManager.S.changeScore(2);
        }
        else
        {
            GameObject sound = Instantiate(brickImpactSound);
            sound.transform.position = collision.gameObject.transform.position;
        }
    }

    private void Recall()
    {
        // Decrement balls here if needed
        this.transform.parent = paddle;
        rb.isKinematic = true;
        inPlay = false;
        gameObject.transform.localPosition = initPos;
    }

    private IEnumerator PaddleCooldown()
    {
        float duration = 0;

        Color currColor;
        paddleMat.SetColor("_Color", coolPaddleColor);

        while (duration <= cooldownMaxTime)
        {
            currColor = Color.Lerp(coolPaddleColor,initPaddleColor, duration / cooldownMaxTime);
            paddleMat.SetColor("_Color", currColor);
            duration += 0.01f;
            yield return new WaitForSecondsRealtime(0.01f);
        }

        canRecall = true;

        // Blink when ready
        duration = 0;
        while (duration <= 0.05f)
        {
            currColor = Color.Lerp(initPaddleColor, Color.white, duration / 0.05f);
            paddleMat.SetColor("_Color", currColor);
            duration += 0.01f;
            yield return new WaitForSecondsRealtime(0.01f);
        }
        duration = 0;
        while (duration <= 0.05f)
        {
            currColor = Color.Lerp(Color.white, initPaddleColor, duration/ 0.05f);
            paddleMat.SetColor("_Color", currColor);
            duration += 0.01f;
            yield return new WaitForSecondsRealtime(0.01f);
        }
        

    }
}
